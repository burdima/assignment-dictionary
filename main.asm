section .text
%define STDIN 0
%define STDOUT 1
%define STDERR 2
%define BUFFER_SIZE 256
%define ERR_EXIT 1

global _start

extern read_word
extern print_string
extern find_word
extern print_newline
section .data

%include 'colon.inc'
%include 'words.inc'

read_word_ok:
db 'read_word ok', 10, 0

read_word_error:
db 'read_word error', 10, 0

find_word_ok:
db 'found key in dictionary: ', 0

find_word_not_found:
db 'key not found', 10, 0

section .text

_start:
    sub rsp, BUFFER_SIZE
    mov rsi, BUFFER_SIZE
    mov rdi, rsp
    call read_word
    test rax, rax
    jz .error_read

    mov rdi, rax
    mov rsi, entry
    call find_word
    test rax, rax
    jz .error_find
    push rax
    mov rdi, find_word_ok
    mov rsi, STDOUT
    call print_string
    pop rax
    mov rdi, rax
    mov rsi, STDOUT
    call print_string
    call print_newline
    xor rdi, rdi
    call exit

.error_read:
    mov rdi, read_word_error
    mov rsi, STDERR
    call print_string
    mov rdi, ERR_EXIT
    call exit
.error_find:
    mov rdi, find_word_not_found
    mov rsi, STDERR
    call print_string
    mov rdi, ERR_EXIT
    call exit
