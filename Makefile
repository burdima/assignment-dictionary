lib.o: lib.asm
	nasm -f elf64 -o lib.o lib.asm

dict.o: dict.asm lib.o
	nasm -f elf64 -o dict.o dict.asm

main.o: main.asm dict.o words.inc
	nasm -f elf64 -o main.o main.asm

main: main.o lib.o dict.o
	ld -o main main.o lib.o dict.o	

clean:
	rm -rf *.o
	rm -rf main

.PHONY: clean
