section .text

%define SYS_READ 0
%define SYS_WRITE 1
%define SYS_EXIT 60

%define STDOUT_DESCRIPTOR 1
%define STDIN_DESCRIPTOR 0

%define TAB 0x9


; Принимает код возврата и завершает текущий процесс
exit:
        mov rax, SYS_EXIT ; номер системного вызова
        syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
        xor rax, rax
.loop:
        cmp byte[rdi+rax], 0
        je .end
        inc rax
        jmp .loop
.end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
        call string_length
        mov rsi, rdi;
        mov rdx, rax
        mov rax, SYS_WRITE
        mov rdi, STDOUT_DESCRIPTOR 
        syscall
        xor rax,rax ;
        ret

; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
        test rdi, rdi
        js .negative
        jmp print_uint
.negative:
        mov rax, rdi
        push rax
        mov rdi, '-'
        call print_char
        pop rax
        neg rax
        mov rdi, rax


; Выводит беззнаковое 8-байтовое число в десятичном формате
print_uint:
        mov r8, rsp
        push 0
        mov rax, rdi
        mov r9, 10

.loop:
        xor rdx, rdx
        div r9
        add rdx, '0'
        dec rsp
        mov [rsp], dl
        cmp rax, 0
        ja .loop

        mov rdi, rsp
        call print_string
        mov rsp, r8
        ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
        mov rdi, '\n'

; Принимает код символа и выводит его в stdout
print_char:
        push rdi
        mov rsi, rsp
        mov rax, SYS_WRITE
        mov rdi, STDOUT_DESCRIPTOR
        mov rdx, 1
        syscall
        pop rdi
        ret



; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
          xor rcx, rcx
.next_char:
    mov r8b, [rdi+rcx]
    mov r9b, [rsi+rcx]
    cmp r8b, r9b
    jne .different
    test r8b, r8b
    jz .equals
    inc rcx
    jmp .next_char
.different:
    mov rax, 0
    ret
.equals:
    mov rax, 1
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
        push 0
        mov rax, SYS_READ
        mov rdi, STDIN_DESCRIPTOR
        mov rsi, rsp
        mov rdx, 1
        syscall
        pop rax
        ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
        mov r9, rsi
        push rdi
        push rsi

.space_loop:
        call read_char
        cmp rax, TAB
        je .space_loop
        cmp rax, ' '
        je .space_loop
        cmp rax, '\n'
        je .space_loop
        pop rsi
        pop rdi
        xor r8, r8

.symbol_loop:
        test rax, rax
        jz .break
        cmp rax, TAB
        je .break
        cmp rax, ' '
        je .break
        cmp rax, '\n'
        je .break
        cmp r8, r9
        jae .over
        mov [rdi+r8], rax
        inc r8
        push rdi
        push rsi
        call read_char
        pop rsi
        pop rdi
        jmp .symbol_loop

.break:
        mov rax, rdi
        mov rdx, r8
        ret

.over:
        xor rax, rax
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
        dec rdi
        xor rax, rax
        xor rdx, rdx
        xor rcx, rcx
        mov r8, 10

.loop:
        inc rdi
        mov al, byte[rdi]
        test rax, rax
        je .end
        cmp rax, '0'
        jb .end
        cmp rax, '9'
        ja .end
        sub rax, '0'
        push rax
        mov rax, rcx
        push rdx
        mul r8
        pop rdx
        mov rcx, rax
        pop rax
        add rcx, rax
        inc rdx
        jmp .loop

.end:
        test rdx, rdx
        je .empty
        mov rax, rcx
        ret

.empty:
        xor rax, rax
        xor rdx, rdx
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
        cmp byte[rdi], '-'
        je .negative

.positive:
        call parse_uint
        test rdx, rdx
        je .empty
        ret

.negative:
        inc rdi
        call parse_uint
        test rdx, rdx
        je .empty
        neg rax
        inc rdx
        ret

.empty:
        xor rdx, rdx
        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0

string_copy:
    call string_length  
    cmp rdx, rax
    jb .else
    mov rcx, rax
.loop:
    test rcx, rcx
    je .break
    dec rcx              
    mov dl, byte [rdi + rcx]   
    mov byte [rsi + rcx], dl
    jmp .loop
.break:
    mov byte [rsi + rax], 0
    ret
.else:
    xor rax, rax
    ret
